Earthquake Geoserve
===================


This project contains much of the CI/CD configuration for the
earthquake-geoserve-ui and earthquake-geoserve-ws web applications.

The test/build Jenkinsfile is found in the public GitHub project for the UI
(https://github.com/usgs/earthquake-geoserve-ui/blob/master/Jenkinsfile).

