export DEBUG='true';

##
# Configuration for UI service
##
export UI_IMAGE_NAME=${UI_IMAGE_NAME:-'ghsc/hazdev/earthquake-geoserve/ui:latest'};
export UI_MOUNT_PATH=${UI_MOUNT_PATH:-'/geoserve'};

case ${TARGET_HOSTNAME} in
  dev*)
    SITE_URL='https://dev-earthquake.cr.usgs.gov';
    ;;
  *)
    SITE_URL='https://earthquake.usgs.gov';
    ;;
esac
export SITE_URL=${SITE_URL};

##
# Configuration for WS service
##
export WS_IMAGE_NAME=${WS_IMAGE_NAME:-'ghsc/hazdev/earthquake-geoserve/ws:latest'};
export WS_DB_PASSWORD=${WS_DB_PASSWORD:-'readonly'};
export WS_DB_USER=${WS_DB_USER:-'web'};
export WS_MOUNT_PATH=${WS_MOUNT_PATH:-'/ws/geoserve'};

##
# Configuration for DB service
##
export DB_IMAGE_NAME=${DB_IMAGE_NAME:-'ghsc/hazdev/earthquake-geoserve/db:latest'};
export DB_ADMIN_PASSWORD=${DB_ADMIN_PASSWORD:-'letmein'};
export DB_ADMIN_USER=${DB_ADMIN_USER:-'postgres'};
export DB_DATADIR_CONTAINER=${DB_DATADIR_CONTAINER:-'/var/lib/postgresql/data/pgdata'};
export DB_LOAD_TYPE=${DB_LOAD_TYPE:-'incremental'};
export DB_NAME=${DB_NAME:-'earthquake'};
export DB_SCHEMA=${DB_SCHEMA:-'geoserve'};

##
# Configuration based on other values previously defined
##
# Note: "db" is the name of the service in the .yml file
export WS_DB_DSN="pgsql:host=db;port=5432;dbname=${DB_NAME}";

# Used by data loader process to create user, schema, and load data
# During initdb.d script runtime (when this value is used), the database
# is only listening on a unix socket. For this reason, remove host/port
# from the DSN and things seem to work again.
#
# Ref: https://github.com/docker-library/postgres/commit/6fe8c15843400444e4ba6906ec6f94b0d526a678
export DB_ADMIN_DSN="pgsql:dbname=${DB_NAME}";

# Define the service URL paths to proxy by hazdev-router
export SERVICE_MAP=(
  "${UI_MOUNT_PATH}:ui"
  "${WS_MOUNT_PATH}:ws"
);