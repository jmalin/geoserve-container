#!/bin/bash -ex


CONFIGS="\
  build-ui/config.xml
  build-ws/config.xml
  deploy-ui/config.xml
"

function sync () {
  config=$1;
  host='igskcicgvmjnkpd.cr.usgs.gov';
  path="/var/lib/jenkins/jobs/earthquake-geoserve/jobs";
  # Uncomment the next line to to a test run instead
  #dryrun='--dry-run';

  rsync -avz -e ssh \
    --checksum \
    --progress \
    ${dryrun} \
    ${host}:${path}/${config} ${config};
}


pushd $(dirname $0) > /dev/null 2>&1;

for config in $CONFIGS; do
  sync $config;
done

popd > /dev/null 2>&1;

exit 0;
